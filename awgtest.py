"""
Run a series of excitations on the large test stand at livingston.
"""
import sys
import awg
import time

if len(sys.argv) > 1:
    chan = sys.argv[1]
else:
    chan = "X2:LSC-AA_SASY90_EXC"

if len(sys.argv) > 2:
    flag = sys.argv[2]
else:
    flag = ""

if flag == "-f":
    full = True
else:
    full = False

print(f"Testing on channel '{chan}'")

if full:
    # Sine wave
    print("Starting Sine")
    sin_exc = awg.Sine(chan, freq=2, ampl=100)
    sin_exc.start()
    time.sleep(2)
    sin_exc.stop()
    print("Stopping Sine")

    time.sleep(1)


    # triangle
    print("Starting Triangle")
    tri_exc = awg.Triangle(chan, freq=2, ampl=100)
    tri_exc.start()
    time.sleep(2)
    tri_exc.stop()
    print("Stopping Triangle")

    time.sleep(1)

    # square
    print("Starting Square")
    square_exc = awg.Square(chan, freq=2, ampl=100)
    square_exc.start()
    time.sleep(2)
    square_exc.stop()
    print("Stopping Square")

    time.sleep(1)

    # sawtooth
    print("Starting Sawtooth")
    saw_exc = awg.Ramp(chan, freq=2, ampl=100)
    saw_exc.start()
    time.sleep(2)
    saw_exc.stop()
    print("Stopping Sawtooth")

    time.sleep(1)

    # spike
    print("Starting Spikes")
    spike_exc = awg.Impulse(chan, freq=2, ampl=100)
    spike_exc.start()
    time.sleep(2)
    spike_exc.stop()
    print("Stopping Spikes")

    time.sleep(1)

    # uniform noise
    print("Starting Uniform Noise")
    uni_exc = awg.UniformNoise(chan, ampl=100)
    uni_exc.start()
    time.sleep(2)
    uni_exc.stop()
    print("Stopping Uniform Noise")

    time.sleep(1)

    # gaussian noise
    print("Starting Gaussian Noise")
    gauss_exc = awg.GaussianNoise(chan, ampl=100)
    gauss_exc.start()
    time.sleep(2)
    gauss_exc.stop()
    print("Stopping Gaussian Noise")

    time.sleep(1)

    # composite
    print("Starting Noisy Sine")
    comp_exc = awg.Excitation.compose([gauss_exc, sin_exc])
    comp_exc.start()
    time.sleep(2)
    comp_exc.stop()
    print("Stopping Noise Sine")

    time.sleep(1)

def collatz(n):
    """return number of steps to reach 1 following collatz"""
    if n <= 1:
        return 0
    elif n%2 == 0:
        return 1 + collatz(n//2)
    else:
        return 1 + collatz(3*n + 1)

# arbitrary
print("start arbitrary")
col_series = [collatz(i+1) for i in range(2**15)]
col_exc = awg.ArbitraryStream(chan, rate=2**14)
col_exc.send(col_series)
print("done arbitrary")

time.sleep(1)

# arbitrary loop
print("Stopping loop")
loop_exc = awg.ArbitraryLoop(chan, col_series, rate=2**14)
loop_exc.start()
time.sleep(4)
loop_exc.stop()
print("Stopping loop")
time.sleep(1)

print("Done")