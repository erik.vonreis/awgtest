"""
Run an excitation on 9 channels at once.
"""
import sys
import awg
import time
from multiprocessing import Process
from gpstime import gpstime

chans = "X2:ALS-C_COMM_A_LF_EXC " \
        "X2:ALS-C_COMM_A_RF_ERR_EXC " \
        "X2:ALS-C_COMM_PLL_CTRL_EXC " \
        "X2:ALS-C_COMM_PLL_ERR_EXC " \
        "X2:ALS-C_DIFF_A_LF_EXC " \
        "X2:ALS-C_DIFF_PLL_ERR_EXC " \
        "X2:ALS-C_REFL_DC_BIAS_EXC " \
        "X2:ALS-C_REFL_DC_ERR_EXC " \
        "X2:ALS-C_X_PDH_CTRL_EXC".split()

def run_sin(chan, target_s):

    print(f"running sin on {chan}")
    sin_exc = awg.Sine(chan, start=target_s, freq=2, duration=2, ampl=100)
    n = gpstime.gps(gpstime.now())
    sin_exc.start()
    time.sleep(2)
    sin_exc.stop()
    # new_targ = target_s + 20
    # sin_exc = awg.Sine(chan, start=new_targ, freq=2, ampl=100)
    # n = gpstime.gps(gpstime.now())
    # sin_exc.start()
    # time.sleep(2)
    # sin_exc.stop()


print(f"Testing on channels {' '.join(chans)}")

# Sine wave
print("Starting Sine")
procs = []
targ = gpstime.gps(gpstime.now()) + 10
for chan in chans:
    p = Process(target=run_sin, args = (chan, targ))
    p.start()
    procs.append(p)

time.sleep(1)

for p in procs:
    p.join()

print("Stopping Sine")


# def collatz(n):
#     """return number of steps to reach 1 following collatz"""
#     if n <= 1:
#         return 0
#     elif n%2 == 0:
#         return 1 + collatz(n//2)
#     else:
#         return 1 + collatz(3*n + 1)
#
# # arbitrary
# print("start arbitrary")
# col_series = [collatz(i+1) for i in range(2**15)]
# for chan in chans:
#     col_exc = awg.ArbitraryStream(chan, rate=2**14)
#     col_exc.send(col_series)
# print("done arbitrary")
#
# time.sleep(1)

print("Done")