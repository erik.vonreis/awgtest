"""
Run a series of excitations on the large test stand at livingston.
"""
import sys
import awg
import time

if len(sys.argv) > 1:
    chan = sys.argv[1]
else:
    chan = "X2:LSC-AA_SASY90_EXC"


# Sine wave
print("Starting Sine")
sin_exc = awg.Sine(chan, freq=2, ampl=100)
sin_exc.start()
time.sleep(2)
sin_exc.stop()
print("Stopping Sine")

time.sleep(1)

print("Done")